// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.

const functions = require(`firebase-functions`);
const express = require('express');
const cors = require('cors');
const config = functions.config();
const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
// Expose Express API as a single Cloud Function:
exports.widgets = functions.https.onRequest(app);
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require(`firebase-admin`);
admin.initializeApp(config.firebase);

function yyyy_mm_dd_h_m_s() {
  var now = new Date();  
  var mm = now.getMonth() + 1; // getMonth() is zero-based
  var dd = now.getDate();
  var h = now.getHours();
  var m = now.getMinutes();
  var s = now.getSeconds();

  return [now.getFullYear() + `-` ,
          (mm>9 ? `` : `0`) + mm + `-`,
          (dd>9 ? `` : `0`) + dd+ `[`,
          (h>9 ? `` : `0`) + h+ `:`,
          (m>9 ? `` : `0`) + m+ `:`,
          (s>9 ? `` : `0`) + s + `]`,
         ].join(``);
};

function yyyy_mm_dd() {
  var now = new Date();  
  var mm = now.getMonth() + 1; // getMonth() is zero-based
  var dd = now.getDate();

  return [now.getFullYear() + `_` ,
          (mm>9 ? `` : `0`) + mm + `_`,
          (dd>9 ? `` : `0`) + dd,
         ].join(``);
};

function specialCharRemove(str:string){
  var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
  if(regExp.test(str)){
    return str.replace(regExp, "");
  }
  return str;
}

async function getValueFromDatabase(path:string){
  try{
    const v = await admin.database().ref(path).once('value');
    return v.val();
  }
  catch{
    return null;
  }
}

async function getMultiValueFromDatabase(path:string){
  try{
    const v = await admin.database().ref(path).once('value');
    return v.val();
  }
  catch{
    return null;
  }
}

async function updateToDatabase(path:string, value:any){
  try {
    return await admin.database().ref(path).update(value);
  }
  catch {
    return null;
  }
}

async function setToDatabase(path:string, value:any){
  try {
    return await admin.database().ref(path).set(value);
  }
  catch {
    return null;
  };
}

async function pushToDatabase(path:string, value:any){
  try {
    return await admin.database().ref(path).push(value);
  }
  catch {
    return null;
  };
}

async function buildNewUserData(user:any){
    console.log("called createUserDataToDatabase: " + user.name);
    const u = user.uid;
    const d = {
      "name": user.name,
      "email": user.email
    };
    const d2 = {
      "name": user.name,
      "email": user.email,
      "uid": user.uid
    };

    const defaultUserData = {
      name: user.name,
      email: user.email,
      uid: user.uid,
      averagePPR: 0,
      grade: "N",
      highOut: 0,
      outSuccessPercent: 0,
      tonOutSuccessPercent: 0,
      ton80Count: 0,
      tonCount: 0,
      totalGameCount: 0,
      winPercentage: 0,
      winCount: 0,
      loseCount: 0,
      outChance: 0,
      outSuccess: 0,
      tonOutChance: 0,
      tonOutSuccess: 0
    };
    const _path = "/uidToUserData/"+u+"/";
    const _path2 = "/nameEmailToUserData/"+specialCharRemove(user.name)+"_"+specialCharRemove(user.email)+"/";
    const _path3 = "/userReportSummary/"+u+"/";
    
    const chk1 = await setToDatabase(_path, d);
    const chk2 = await setToDatabase(_path2, d2);
    const chk3 = await setToDatabase(_path3, defaultUserData);
    if(chk1 && chk2 && chk3){
      return true;
    }
    return false;
}


async function findUserByName(name:string){
  const _name = specialCharRemove(name);
  const _nameDb = await getValueFromDatabase('/nameEmailToUserData');
  const _keys = Object.keys(_nameDb);
  var _resFindUserByName:any = [];
  if(!!_nameDb){
    _keys.forEach((k:string)=>{
      var n = k.split('_');
      if(n[0].includes(_name)){
        _resFindUserByName.push(_nameDb[k]);
      }
    });
    return _resFindUserByName;
  }
  return null;
}

async function getUserSummaryByUid(uid: string){
  console.log("called getUserSummaryByUid : " + uid);
  const _path = "/userReportSummary/"+uid+"/";
  const _summaryReport = await getValueFromDatabase(_path);
  if(!!_summaryReport) return _summaryReport;
  return null;
} 

class GamesModel {
  key: string;
  game: any;
  constructor(){
    this.key = "";
    this.game = null;
  }
}

class DateGamesModel {
  date: string;
  game: Array<any>;
  constructor(){
    this.date = "";
    this.game = [];
  }
}

async function getUserReportByUid(recentLen: number, dateLen:number, uid: string){
  console.log("called getUserReportByUid : " + uid);
  // get recently 30 games
  var recently30GamePPR = [];
  const recentGamePath = "/userReportWithHistory/"+ uid + "/";
  const recentGames = await getValueFromDatabase(recentGamePath);
  const recentGameKeys = Object.keys(recentGames);
  const recentGameLength = recentGameKeys.length;
  var targetLength = 0;
  for(var i = recentGameLength-1 ; i >= 0; i--){
    recently30GamePPR.push(recentGames[recentGameKeys[i]].roundAveragePPR);
    targetLength++;
    if(recentLen < targetLength) break;
  }

  // get date
  var byDateGamesResult = [];

  const byDateGamePath = "/userReportWithDate/"+ uid + "/";
  const byDateGames = await getValueFromDatabase(byDateGamePath);
  const byDateGameKeys = Object.keys(byDateGames);
  const byDateGameLength = byDateGameKeys.length;
  var targetDateLength = 0;
  for(var d = byDateGameLength-1 ; d >= 0; d--){
    var obj = new DateGamesModel;
    obj.date = byDateGameKeys[d];
    // obj.game = byDateGames[byDateGameKeys[d]];
    var dayKeys = Object.keys(byDateGames[byDateGameKeys[d]]);
    dayKeys.forEach(k=>{
      var g = new GamesModel;
      g.key = k;
      g.game = byDateGames[byDateGameKeys[d]][k];
      obj.game.push(g);
    });
    byDateGamesResult.push(obj);
    targetDateLength++;
    if(dateLen < targetDateLength) break;
  } 
  // get date 
  return {recentPPRs: recently30GamePPR, dateGames:byDateGamesResult};
}

async function _calcGrade(data:any){
  var g0:number = 0;
  var g1:number = 0;
  var g2:number = 0;
  
  // 33.33% is PPR
  // 33.33% is WinPercent
  // 33.33% is OutPercent

  g0 = (data.averagePPR * 0.7);
  g1 = (data.winPercentage * 0.15);
  g2 = (data.outSuccessPercent * 0.15);

  const gradeVolume:number = (g0 + g1 + g2);
  if(gradeVolume >= 90){
    return "SS";
  }
  else if((gradeVolume <90) && (gradeVolume >= 80)){
    return "S";
  }
  else if((gradeVolume <80) && (gradeVolume >= 70)){
    return "A";
  }  
  else if((gradeVolume <70) && (gradeVolume >= 60)){
    return "B";
  }
  else if((gradeVolume <60) && (gradeVolume >= 50)){
    return "C";
  }
  else{
    return "N";
  }
}

async function gameDataWithUid(gData:any){
  // resultData not requierd
  delete gData.resultData;

  console.log("called gameDataWithUid" + JSON.stringify(gData));

  // save to user/date
  let _userDate = await pushToDatabase("/userReportWithDate/"+gData.uid+"/"+yyyy_mm_dd()+"/", gData);
  await setToDatabase("/userReportWithHistory/"+gData.uid+"/"+_userDate.getKey()+"/", gData);

  var _summaryDb = await getValueFromDatabase("/userReportSummary/"+gData.uid+"/");

  if(!!_summaryDb){
    _summaryDb.winCount += gData._winCount;
    _summaryDb.loseCount += gData._loseCount;
    _summaryDb.totalGameCount = _summaryDb.winCount + _summaryDb.loseCount;
    // calc winning percentage 
    _summaryDb.winPercentage = ((_summaryDb.winCount/_summaryDb.totalGameCount)*100).toFixed(2);

    _summaryDb.outChance += gData.outChance;
    _summaryDb.outSuccess += gData.outSuccess;
    _summaryDb.tonOutChance += gData.tonOutChance;
    _summaryDb.tonOutSuccess += gData.tonOutSuccess;
    if(_summaryDb.highOut < gData.highOut) _summaryDb.highOut = gData.highOut;
    // calc out success percentage
    if(_summaryDb.outChance > 0){
      _summaryDb.outSuccessPercent = ((_summaryDb.outSuccess/_summaryDb.outChance)*100).toFixed(2);
    }
    if(_summaryDb.tonOutChance > 0){
      _summaryDb.tonOutSuccessPercent = ((_summaryDb.tonOutSuccess/_summaryDb.tonOutChance)*100).toFixed(2);
    }

    // calc PPR
    var _origin = _summaryDb.totalGameCount;
    if(_origin > 20) _origin = 20;
    _summaryDb.averagePPR = ((_summaryDb.averagePPR * ((_origin-1)/_origin)) + (gData.roundAveragePPR * (1/_origin))).toFixed(2);

    // ton & ton80
    _summaryDb.tonCount += gData.roundTon;
    _summaryDb.ton80Count += gData.roundTon80;

    // calc Grade
    _summaryDb.grade = await _calcGrade(_summaryDb);
    // reWrite summary
    await updateToDatabase("/userReportSummary/"+gData.uid+"/", _summaryDb);
    return true;
  }

  return null;
}

async function getPublicVersion(){
  const _version = await getValueFromDatabase('/version/');
  if(!!_version) return _version;
  return null;
}

async function getAdmobId(os:string){
  const _id = await getValueFromDatabase('/admob/'+os);
  if(!!_id) return _id;
  return null;
}

async function getNameByUid(uid:string){
  const _id = await getValueFromDatabase('/uidToUserData/'+uid);
  if(!!_id) return _id.name;
  return null;
}

async function updateOneLineNoticeBoard(data: any){
  console.log("NNNN updateOneLineNoticeBoard : " + JSON.stringify(data));
  data['time'] = yyyy_mm_dd_h_m_s();
  await pushToDatabase("/oneLineNoticeBoard/", data);
  return true;
}

function getOneLineNewerNoticeBoard(data: any): Promise<any>{
  return new Promise((resolve, reject)=>{
    var len = 10;
    admin.database().ref("/oneLineNoticeBoard/").orderByKey()
    .limitToLast(len)
    .once('value')
    .then((snapshot:any)=>{
      resolve(snapshot.val())
    })
    .catch((error:any)=>{
      reject(error);
    });
  });
}

function getOneLineOlderNoticeBoard(data: any): Promise<any>{
  return new Promise((resolve, reject)=>{
    var len = 5;
    admin.database().ref("/oneLineNoticeBoard/").orderByKey()
    .limitToLast(len)
    .endAt(data.token)
    .once('value')
    .then((snapshot:any)=>{
      console.log("NNNNNN log???? : " + JSON.stringify(snapshot.val()));
      resolve(snapshot.val())
    })
    .catch((error:any)=>{
      reject(error);
    });
  });
}
// function format 
// 
//async function functionName(variable:string){
// }

exports.restAPI = functions.https.onRequest(async (request:any, response:any) => {
  //set JSON content type and CORS headers for the response
  response.header('Content-Type','application/json');
  response.header('Access-Control-Allow-Origin', '*');
  response.header('Access-Control-Allow-Headers', 'Content-Type');
  const items = request.url.split("/");
  const requestHandle = items[1];
  const _data = request.body;

  // create new user api
  if(requestHandle==='createNewUser'){
    const _res = await buildNewUserData(_data);
    if(!!_res) return response.status(200).send({result:true});
    return response.status(200).send({result:null});
  } 
  // error log 
  else if(requestHandle==='errorLog'){
    console.error(_data.log);
    return response.status(200).send({result:true});
  }

  // find user item api
  else if(requestHandle==='findUserByName'){
    const _res = await findUserByName(_data.name);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // find user item api
  else if(requestHandle==='findUserResultByName'){
    const _res = await findUserByName(_data.name);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }
  
  // find user summary report api
  else if(requestHandle==='getUserSummaryByUid'){
    const _res = await getUserSummaryByUid(_data.uid);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // save game data api
  else if(requestHandle==='gameDataWithUid'){
    const _res = await gameDataWithUid(_data);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // get version
  else if(requestHandle==='getPublicVersion'){
    const _res = await getPublicVersion();
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // find user report api
  else if(requestHandle==='getUserReportByUid'){
    const _res = await getUserReportByUid(_data.recentLen, _data.dateLen, _data.uid);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  else if(requestHandle==='getAdmobId'){
    const _res = await getAdmobId(_data.os);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  else if(requestHandle==='getNameByUid'){
    const _res = await getNameByUid(_data.uid);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  else if(requestHandle==='updateOneLineNoticeBoard'){
    const _res = await updateOneLineNoticeBoard(_data);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  else if(requestHandle==='getOneLineOlderNoticeBoard'){
    const _res = await getOneLineOlderNoticeBoard(_data);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  else if(requestHandle==='getOneLineNewerNoticeBoard'){
    const _res = await getOneLineNewerNoticeBoard(_data);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // new api format 
  //
  // else if(requestHandle==='example'){}

  // unknown request name 
  else {
    return response.status(400).send({result:"wrong requestHandler"});
  }
});

